var express = require("express");
var knex = require("../config/knex");
var router = express.Router();
var orderid = require('order-id')('mysecret');

router.post('/user/order', function(req, res){
    var list = req.body.list;
    var total = req.body.total;
    var order_id = orderid.generate();
    var email = req.body.email;

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            var now = knex.fn.now();

            return knex.transaction(function(trx){
                return knex('orders')
                .transacting(trx)
                .insert({
                    number: order_id,
                    user_id: user_id,
                    total: total,
                    created_at: now
                })
                .then(function(created){
                    var order = created[0];
                    var sql = "";
                    var sqlDeleteCart = "";
                    var sqlDeleteProposal = "";
                    for(var i = 0; i < list.length; i++){
                        var item = list[i];
                        var exchange_device_id = null;
                        if(item.proposal_id && item.cart_type === 2)
                            exchange_device_id = item.exchange_proposal_real_device_id;
                        var transaction_id = orderid.generate();
                        sql += `(null, '${transaction_id}', ${item.device_id}, ${exchange_device_id}, ${user_id}, ${order}, ${item.real_price}, ${item.cart_type}, 'created', null,${now})`;
                        if(i !== 0){
                            sqlDeleteCart += ' OR ';
                            sqlDeleteProposal += ' OR ';
                        }
                        sqlDeleteCart += `id=${item.id}`;
                        sqlDeleteProposal += `(user_id=${user_id} AND cart_id=${item.id})`;
                        if(i < list.length-1)
                            sql += `, `;
                    }
                    sql = 'INSERT INTO transactions VALUES '+sql;
                    sqlDeleteCart = 'DELETE FROM carts WHERE '+sqlDeleteCart;
                    sqlDeleteProposal = 'DELETE FROM proposals WHERE '+sqlDeleteProposal;
                    return knex.raw(sql)
                    .transacting(trx)
                    .then(() => {
                        return knex.raw(sqlDeleteCart)
                        .transacting(trx)
                        .then(() => {
                            return knex.raw(sqlDeleteProposal)
                            .transacting(trx)
                            .then(trx.commit)
                            .catch(trx.rollback)
                        })
                        .catch(trx.rollback)
                    })
                    .catch(trx.rollback)
                })
                .catch(trx.rollback);
            })
        }else{
            res.json({status: 500});
        }
    })
    .then(function(response){
        res.json({data: '', status: 200});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/user/order/list', function(req, res){
    var email = req.body.email;
    
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `
                SELECT od.id as id,
                od.number as number,
                od.user_id as user_id,
                od.total as total,
                strftime('%d/%m/%Y', od.created_at) AS created_at
                FROM orders od
                WHERE od.user_id = `+user_id+`
                ORDER BY od.created_at DESC
                `
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/user/transaction/list', function(req, res){
    var email = req.body.email;
    
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `
                SELECT ts.number AS number
                , ts.device_id AS device_id
                , ts.exchange_device_id AS exchange_device_id
                , ts.user_id AS user_id, ts.order_id AS order_id
                , ts.price AS price
                , ts.type AS type
                ,ts.status AS status
                ,strftime('%d/%m/%Y', ts.created_at) AS created_at
                ,devices.device_name AS device_name,
                orders.number as order_number,
                exchange_devices.device_name AS exchange_device_name
                FROM transactions as ts
                INNER JOIN (
                    SELECT devices.id, imeis.device_name
                    FROM devices
                    INNER JOIN (
                        SELECT imeis.imei, models.name AS device_name
                        FROM imeis
                        INNER JOIN models ON models.id = imeis.model_id
                    ) AS imeis ON imeis.imei = devices.imei
                ) AS devices ON devices.id = ts.device_id
                INNER JOIN orders ON orders.id = ts.order_id
                LEFT OUTER JOIN (
                    SELECT devices.id, imeis.device_name
                    FROM devices
                    INNER JOIN (
                        SELECT imeis.imei, models.name AS device_name
                        FROM imeis
                        INNER JOIN models ON models.id = imeis.model_id
                    ) AS imeis ON imeis.imei = devices.imei
                ) AS exchange_devices ON exchange_devices.id = ts.exchange_device_id
                WHERE ts.user_id = `+user_id+`
                ORDER BY ts.created_at DESC
                `
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
//admin
router.post('/order/list', function(req, res){
    var name = req.body.search.name;
    knex.raw(
        `SELECT
        orders.id, orders.number, orders.user_id, orders.total, orders.created_at, users.email as email
        FROM orders
        INNER JOIN users ON users.id = orders.user_id
        WHERE number LIKE '%`+name+`%'
        ORDER BY number DESC`
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/transaction/list', function(req, res){
    var name = req.body.search.name;
    knex.raw(
        ` SELECT ts.number AS number
        , ts.device_id AS device_id
        , ts.exchange_device_id AS exchange_device_id
        , ts.user_id AS user_id, ts.order_id AS order_id
        , ts.price AS price
        , ts.type AS type
        ,ts.status AS status
        ,strftime('%d/%m/%Y', ts.created_at) AS created_at
        ,devices.device_name AS device_name,
        orders.number as order_number,
        exchange_devices.device_name AS exchange_device_name,
        users.email as email
        FROM transactions as ts
        INNER JOIN (
            SELECT devices.id, imeis.device_name
            FROM devices
            INNER JOIN (
                SELECT imeis.imei, models.name AS device_name
                FROM imeis
                INNER JOIN models ON models.id = imeis.model_id
            ) AS imeis ON imeis.imei = devices.imei
        ) AS devices ON devices.id = ts.device_id
        INNER JOIN orders ON orders.id = ts.order_id
        INNER JOIN users ON users.id = ts.user_id
        LEFT OUTER JOIN (
            SELECT devices.id, imeis.device_name
            FROM devices
            INNER JOIN (
                SELECT imeis.imei, models.name AS device_name
                FROM imeis
                INNER JOIN models ON models.id = imeis.model_id
            ) AS imeis ON imeis.imei = devices.imei
        ) AS exchange_devices ON exchange_devices.id = ts.exchange_device_id
        WHERE ts.number LIKE '%`+name+`%'
        ORDER BY number DESC`
    )
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        console.log("er",error)
        res.status(500).json();
    })
});
module.exports = router;