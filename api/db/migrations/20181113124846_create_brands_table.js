
exports.up = function(knex, Promise) {
    return knex.schema.createTable('brands', function(t){
        t.increments('id').primary();
        t.string('name');
        t.string('image');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('brands');
};
